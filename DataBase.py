import psycopg2
import config
import random

    


class DataBase:

    #Создание подлючения
    def __init__(self):
    
        self.conn = psycopg2.connect(dbname   = config.DB_NAME,
                                user     = config.USER,
                                password = config.PASSWORD,
                                host     = config.HOST,
                                port     = config.PORT)
        self.cursor = self.conn.cursor()

    #Разрыв подключения   
    def disconnect(self):

        self.conn.close()

    #Выборка всех рекламных акций
    def select_adv(self): 

        self.cursor.execute("""SELECT 
                                   adv_id ,adv_img , adv_name , adv_text, adv_link_vk 
                               FROM 
                                   public.adv""")

        rows = self.cursor.fetchall() 

        return rows

    #Выборка броней по определенному пользователю (Убить себя и убрать кросс-джоин)
    def select_reserv(self, user_code):

        self.cursor.execute(f"""SELECT 
                                    user_id
                                FROM
                                    public.user
                                WHERE
                                    user_code = '{user_code}';""")

        user_id = self.cursor.fetchall()[0][0]

        self.cursor.execute(f"""SELECT *
                                FROM
                                    public.reserv
                                WHERE
                                    user_id = {user_id}""")

        struct_reserv = self.cursor.fetchall()
        
        return struct_reserv

    #Выборка услуги по ее ИД 
    def select_service_all(self, service_id):

        self.cursor.execute(f"""SELECT *
                                FROM
                                    public.service
                                WHERE
                                    service_id = {service_id};""")

        struct_service = self.cursor.fetchall()

        return struct_service

    #Выбора ИД услуги по ее имени
    def select_service_id(self, service_name):

        self.cursor.execute(f"""SELECT 
                                    service_id
                                FROM
                                    public.service
                                WHERE
                                    service_name = '{service_name}';""")

        struct_service = self.cursor.fetchall()

        return struct_service

    #Удаление заказа пользователя
    def delete_reserv(self, reserv_id):

        self.cursor.execute(f"""DELETE
                                FROM
                                    public.reserv
                                WHERE
                                    reserv_id = {reserv_id};""")

        self.conn.commit()  

    #Выбора админов в работе
    def select_true_admin(self):

        self.cursor.execute(f"""SELECT 
                                    id_type_messenger, name_admin, link_admin 
                                FROM 
                                    public.admins 
                                WHERE 
                                    admin_status = {True};""")

        row = self.cursor.fetchall()

        return row

    #Выборка ИД пользователя по его коду
    def select_user_id(self, user_link):

        self.cursor.execute(f"""SELECT 
                                    user_id 
                                FROM 
                                    public.user 
                                WHERE 
                                    user_code = '{str(user_link)}'; """)

        row = self.cursor.fetchall()

        return row

    #Вставка сообщения пользователя
    def insert_user_message(self, id, text, time):

        self.cursor.execute(f"""INSERT INTO
                                    public.msg
                                    ( user_id , msg_status , msg_text, msg_time) 
                                VALUES
                                    ( '{id}', '{False}', '{text}', '{time}')
                                    ON CONFLICT DO NOTHING;""")

        self.conn.commit()

    #Вставка заказа пользователя
    def insert_reserv(self, s):

        txt = 'Ожидает обработки'

        self.cursor.execute(f"""INSERT INTO 
                                    public.reserv
                                    (user_id, admin_id, reserv_wish, reserv_status, reserv_time, reserv_type, reserv_service)
                                VALUES
                                    ({s["user_id"]}, {1}, '{s["wish"]}', {s["status"]}, '{s["data_time"]}', '{txt}', {s["service_id"]})
                                ON CONFLICT DO NOTHING;""")
        
        self.conn.commit()
    

    #Получение всех доступных услуг
    def select_service(self):

        self.cursor.execute(f"""SELECT 
                                    service_name, service_cost
                                FROM 
                                    public.service; """)

        row = self.cursor.fetchall()

        return row

    #Вставка данных о пользователе
    def insert_user_profile(self, s): 

        self.cursor.execute(f"""INSERT INTO
                                    public.user
                                    ( type_code , user_name, user_ban, user_code) 
                                VALUES
                                    ({s["type_code"]}, '{s["user_name"]}', {s["user_ban"]}, '{s["user_code"]}')
                                    ON CONFLICT DO NOTHING;""")

        self.conn.commit()

    #Выборка пользователя по коду
    def select_user_profile(self, user_code): 

        self.cursor.execute(f"""SELECT 
                                    user_code
                                FROM 
                                    public.user
                                WHERE
                                    user_code= '{user_code}'; """)
        row = self.cursor.fetchall()

        return row

    #Вставка телефона пользователя
    def insert_user_telephone(self, user_code_from, user_teleph): 

        self.cursor.execute(f"""UPDATE 
                                    public.user 
                                SET 
                                    user_telephone = '{user_teleph}' 
                                WHERE 
                                    user_code = '{user_code_from}'
                                ON CONFLICT DO NOTHING; """)

        self.conn.commit()

    #Получение пользователя по коду
    def select_user_all(self, user_link):

        self.cursor.execute(f"""SELECT *
                                FROM 
                                    public.user 
                                WHERE 
                                    user_code = '{str(user_link)}'; """)

        row = self.cursor.fetchall()

        return row

    #Вставка картинки в ВК
    def insert_img_vk_link(self, img_id , img_link):
      
        self.cursor.execute(f"""UPDATE 
                                    public.adv
                                SET 
                                    adv_link_vk = '{img_link}'
                                WHERE
                                    adv_id = '{img_id}'
                                ON CONFLICT DO NOTHING;""")
        self.conn.commit()
