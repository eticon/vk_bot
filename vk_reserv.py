from vk_api.longpoll import VkLongPoll, VkEventType
import vk_api
import bs4
from vk_api import *
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from datetime import *
from vk_api.upload  import VkUpload
import random
import requests
import config
import DataBase
import re
import os


vk_session = vk_api.VkApi(token = config.VK_TOKEN)
session_api = vk_session.get_api()
longpoll = VkLongPoll(vk_session)

global img_index 
img_index = 0

global index 
index = 0

def get_random():

    return random.randint(0,10000000000)

def input_confirm(user_id):

    global struct_reserv

    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button("Да", color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("Нет", color = VkKeyboardColor.NEGATIVE)

    txt = """Проект закакза:\n\nВаши контакты:\n {}\nУслуга: {}\nДата: {}\nВремя: {}\nПожелания: {}\n\n Все верно?\n """.format(
        struct_reserv["user_name"],
        struct_reserv["service"],
        struct_reserv["data"],
        struct_reserv["time"],
        struct_reserv["wish"])

    session_api.messages.send(message = txt, user_id = user_id, random_id = get_random(),
                              keyboard = keyboard.get_keyboard())

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Нет":

                return menu(event.user_id)

            elif event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "Да":

                database.insert_reserv(struct_reserv)
                return menu(event.user_id)


def input_wish(user_id):

    global struct_reserv
    global save_time
    
    keyboard = VkKeyboard(one_time = True)
    keyboard.add_button("Без пожеланий", color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("В меню", color=VkKeyboardColor.NEGATIVE)

    session_api.messages.send(message = 'Ваши пожелания', user_id = user_id, random_id = get_random(),
                              keyboard=keyboard.get_keyboard())
    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "В меню":

                return menu(event.user_id)

            elif event.type == VkEventType.MESSAGE_NEW and event.to_me:

                regular = r'^[0-9]{2}\:[0-9]{2}$'
                time_   = re.fullmatch(regular, save_time)

                if time_:

                    struct_reserv["time"]      = save_time
                    struct_reserv["status"]    = False
                    struct_reserv["data_time"] = struct_reserv["data"] + " " + struct_reserv["time"]
                    struct_reserv["wish"]      = event.text

                    input_confirm(event.user_id)

                else:

                    session_api.messages.send(message = 'Повторите ввод времени', user_id = event.user_id,
                                              random_id = get_random())
                    input_wish(event.user_id)


def input_time(user_id):

    global struct_reserv
    global save_time

    save_time = None

    keyboard = VkKeyboard(one_time=False)
    keyboard.add_button("16:00");
    keyboard.add_button("17:00");
    keyboard.add_button("18:00");
    keyboard.add_button("19:00");
    keyboard.add_line()
    keyboard.add_button("20:00");
    keyboard.add_button("21:00");
    keyboard.add_button("22:00");
    keyboard.add_button("23:00");
    keyboard.add_line()
    keyboard.add_button("00:00");
    keyboard.add_button("01:00");
    keyboard.add_button("02:00");
    keyboard.add_button("03:00");
    keyboard.add_line()
    keyboard.add_button("В меню", color = VkKeyboardColor.NEGATIVE)

    session_api.messages.send(message = 'Выберите время или введите в формате \"ЧЧ:ММ\"',
                              user_id = user_id, random_id = get_random(),
                              keyboard = keyboard.get_keyboard())
    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "В меню":

                return menu(event.user_id)

            elif event.type == VkEventType.MESSAGE_NEW and event.to_me:

                regular   = r'^[0-9]{2}\.[0-9]{2}$'
                date_     = re.fullmatch(regular, date_save)
                save_time = event.text

                if date_:

                    input_wish(event.user_id)

                else:

                    session_api.messages.send(message = 'Повторите ввод даты', user_id = event.user_id,
                                              random_id = get_random())

                    input_date(event.user_id)

                return menu(event.user_id)


def input_date(user_id):

    date_keyboard = []
    now           = datetime.now()

    global date_save
    date_save = None
    
    for i in range(0, 7):

        string = ""

        if now.day < 10:

            string = "0" + str(now.day)

        else:

            string = str(now.day)

        if now.month < 10:

            string += ".0" + str(now.month)

        else:

            string += "." + str(now.month)

        now = now + timedelta(days = 1)
        date_keyboard.append(string)

    keyboard = VkKeyboard(one_time = False)

    for i in range(0, 4):

        keyboard.add_button(date_keyboard[i])

    keyboard.add_line()

    for i in range(4, 7):

        keyboard.add_button(date_keyboard[i])

    keyboard.add_line()
    keyboard.add_button("В меню", color = VkKeyboardColor.NEGATIVE)
    session_api.messages.send(message = 'Выбери дату или введи свою  в формате \"ДД.ММ\"',
                              user_id = user_id, random_id = get_random(),
                              keyboard = keyboard.get_keyboard())

    while True:

        for event in longpoll.listen():

            global struct_reserv

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == "В меню":

                return menu(event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me:

                if date_save == None:

                    date_save = event.text

                struct_reserv["data"]       = date_save
                struct_reserv["service"]    = str(service).split(" - ")[0].strip()
                struct_reserv["service_id"] = database.select_service_id(struct_reserv["service"])[0][0]
                struct_reserv["user_code"]  = event.user_id
                struct_reserv["user_id"]    = database.select_user_id(struct_reserv["user_code"])[0][0]
                struct_reserv["user_name"]  = database.select_user_all(struct_reserv["user_code"])[0][2]


                input_time(event.user_id)

                return menu(event.user_id)

def input_sevice(user_id):

    global struct_reserv
    global service
    service = None
    
    struct_reserv = []
    struct_reserv = dict.fromkeys(["user_name", "user_code",
                                   "data_time", "wish",
                                   "service", "status",
                                   "service_id", "data",
                                   "time", "user_id"], None)

    serv     = database.select_service()
    keyboard = VkKeyboard(one_time = False)

    for i in range(0, len(serv)):

        button = str(serv[i][0] + "   -   " + str(serv[i][1]) + "₽")
        keyboard.add_button(button)
        keyboard.add_line()

    keyboard.add_button("В меню", color = VkKeyboardColor.NEGATIVE)
    
    session_api.messages.send(message = 'Выберите услугу', user_id = user_id, random_id = get_random(),
                              keyboard = keyboard.get_keyboard())

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me:

                if service == None:

                    service = event.text
                    print(service)

                input_date(event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'В меню':

                menu(event.user_id)

def menu(user_id):

    keyboard = VkKeyboard(one_time = False)
    keyboard.add_button('Заказать услугу', color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button('Позвонить нам/Заказать обратный звонок', color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button('Просмотр заказов', color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button('Чат с администратором', color = VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button('Узнать об акциях', color = VkKeyboardColor.POSITIVE)
    
    session_api.messages.send(message = 'Выберите пункт меню:', user_id = user_id,
                              random_id = get_random(), keyboard = keyboard.get_keyboard())

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Заказать услугу':

                input_sevice(event.user_id)
                break

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Просмотр заказов':
               
                view_reserv(event.user_id)
                break

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Позвонить нам/Заказать обратный звонок':
                
                callbacker(event.user_id)
                break

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Чат с администратором':
                logistic(event.user_id)
                break

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Узнать об акциях':
               
                adv(event.user_id)
                break
            
        session_api.messages.send(message = 'Выберите пункт меню:', user_id = user_id,
                                  random_id = get_random(), keyboard = keyboard.get_keyboard())

def callbacker(user_id):

    keyboard = VkKeyboard(one_time=False)
    keyboard.add_button('Позвонить нам', color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button('Заказать обратный звонок', color=VkKeyboardColor.POSITIVE)
    keyboard.add_line()
    keyboard.add_button("В меню", color=VkKeyboardColor.NEGATIVE)

    session_api.messages.send(message = 'Выберите пункт меню:', user_id = user_id, random_id = get_random(),
                                          keyboard = keyboard.get_keyboard())
    
    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Позвонить нам':

                callbacker_callme(event.user_id)

            elif event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'В меню':

                menu(event.user_id)

            elif event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Заказать обратный звонок':
                
                callbacker_call_to_me(event.user_id)

            break
                
                
def starter():

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me:

                user_data = dict.fromkeys(["user_name", "type_code", "user_ban", "user_code"])
                aut       = session_api.users.get(user_ids = event.user_id)
                aut2      = aut[0]

                user_data["user_name"] = aut2['first_name'] + " " + aut2['last_name']
                user_data["type_code"] = 2
                user_data["user_ban"]  = False
                user_data["user_code"] = event.user_id

                user_in_db = database.select_user_profile(event.user_id)

                if not user_in_db:

                    database.insert_user_profile(user_data)

                return menu(event.user_id)


def callbacker_callme(user_id):
    
    session_api.messages.send(message = 'Наш телефон', user_id = user_id, random_id = get_random())
    menu(user_id)
                
def callbacker_call_to_me(user_id):

    session_api.messages.send(message = 'Введите номер телефона', user_id = user_id, random_id = get_random())

    while True:

        for event in longpoll.listen():

            if event.type == VkEventType.MESSAGE_NEW and event.to_me:

                regular = r'^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$'
                phone   = re.fullmatch(regular, event.text)

                if phone:

                    database.insert_user_telephone(event.user_id, event.text)
                
                    session_api.messages.send(message='Телефон введен успешно', user_id=event.user_id,
                                            random_id=get_random())
                    return menu(user_id)

                else:

                    session_api.messages.send(message='Повторите ввод телефона', user_id=event.user_id, random_id=get_random())

                    callbacker_call_to_me(user_id)

def adv(user_id):

    global img_index

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Далее', color=VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Назад', color=VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color=VkKeyboardColor.NEGATIVE)

    row  = database.select_adv()
    ln = len(row)
    slise(row, user_id)
            
    while True:

        for event in longpoll.listen():
             
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Далее':

                img_index = img_index + 1  

                if img_index == ln or ln == 1 : 

                    img_index = 0

                slise(row , event.user_id)
                
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Назад':

                img_index = img_index - 1 

                if img_index == -1 or ln == 1:

                    img_index = ln - 1

                slise(row , event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Меню':

                menu(event.user_id)

def slise(row, user_id):

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color=VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Далее', color=VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color=VkKeyboardColor.NEGATIVE)

    global img_index 
        
    save_photo(row[img_index][0], row[img_index][1])

    session_api.messages.send(message = str(row[img_index][2] + '\n' + row[img_index][3] ),
                                          peer_id = user_id,
                                          random_id = get_random(),
                                          keyboard = slise_keyboard.get_keyboard(),
                                          attachment = photo(row[img_index][0], user_id) )
    
def save_photo(index , tr):

    maindir = os.listdir(path = os.getcwd())
    print(os.getcwd)
    
    try:

        maindir.index('img')
        print(os)

    except ValueError:

        os.mkdir('img')

    try:

        photo_list = os.listdir(path = 'img\\')
        photo_list.index(f'{index}.png')
        
    except ValueError:
        
        f = open(f'img\\{index}.png', 'wb')
        f.write(bytearray(tr))
        f.close()



def photo(img_name , user_id):

    a = vk_session.method("photos.getMessagesUploadServer")
    b = requests.post(a['upload_url'], files = {'photo': open(f'img\\{img_name}.png', 'rb')}).json()
    c = vk_session.method('photos.saveMessagesPhoto', {'photo': b['photo'], 'server': b['server'], 'hash': b['hash']})[0]
   
    return f'photo{c["owner_id"]}_{c["id"]}'


def view_reserv(user_id):

    global index

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Отмена', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)

    row  = database.select_reserv(user_id)
    ln   = len(row)
    slise_reserv(row, user_id)
            
    while True:

        for event in longpoll.listen():
             
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Далее':

                index = index + 1  

                if index == ln or ln == 1 : 

                    index = 0

                slise_reserv(row , event.user_id)
                
            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Назад':

                index = index - 1 

                if index == -1 or ln == 1:

                    index = ln - 1

                slise_reserv(row , event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Меню':

                menu(event.user_id)

            if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text == 'Отмена':

                database.delete_reserv(row[index][0])

                index = index - 1
                ln    = ln - 1

                if index == -1 or ln == 1: 

                    index = ln - 1

                elif index == ln or ln == 1 : 

                    index = 0

                row  = database.select_reserv(user_id)
                print_cancel(event.user_id)

            print(index)

def slise_reserv(row, user_id):

    slise_keyboard = VkKeyboard(one_time = False)
    slise_keyboard.add_button('Назад', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_button('Отмена', color = VkKeyboardColor.PRIMARY)
    slise_keyboard.add_button('Далее', color = VkKeyboardColor.POSITIVE)
    slise_keyboard.add_line()
    slise_keyboard.add_button('Меню', color = VkKeyboardColor.NEGATIVE)
    
    global index

    number    = row[index][0]
    date_time = row[index][4]
    service   = row[index][-1]
    service   = database.select_service_all(service)
    wish      = row[index][3] 
        
    txt = f"Заказ #{number}\nДата: {date_time}\nУслуга: {service[0][1]}\nПожелание: {wish}"

    session_api.messages.send(message = txt,
                              peer_id = user_id,
                              random_id = get_random(),
                              keyboard = slise_keyboard.get_keyboard())

def print_cancel(user_id):

    session_api.messages.send(message = "Заказ отменен",
                              peer_id = user_id,
                              random_id = get_random())


def logistic(user_id):

    session_api.messages.send(message = 'Введите сообщение:',
                                        user_id = user_id, 
                                        random_id = get_random(), 
                                        )
    for event in longpoll.listen():

        if event.type == VkEventType.MESSAGE_NEW and event.to_me and event.text:
            
           row = database.select_user_id(event.user_id)
           database.insert_user_message(row[0][0], event.text, str(datetime.strftime(datetime.now(), "%d.%m %H:%M")))
           menu(user_id)

if __name__ == "__main__":
    database = DataBase.DataBase()
    starter()
    database.disconnect()
